import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from "router/routes";
import CabecalhoSistema from 'componentes/CabecalhoSistema';
import styled from "styled-components";

const InputGabardo = styled.input`
  background: orange;

  ${props => {
    if (props.ligado) {
      return `
        background: green;
      `
    }
  }}
`

function App() {
  return (
    <div>
      <CabecalhoSistema />
      <InputGabardo ligado={false} type="password"/>
      <Router>
        <Switch>
          {routes.map((route) => (
            <Route
              key={route.path}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          ))}
        </Switch>
      </Router>
    </div>
  );
}

export default App;
