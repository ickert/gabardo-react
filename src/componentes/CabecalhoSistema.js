import React from 'react'
import styled from "styled-components";

const Cabecalho = styled.div`
  height: 44px;
  border-radius: 4px;
  background-color: #3cb624;
  color: #fff;
  margin-bottom: 20px;
  animation: 2s;
  display: flex;
`;
const CabecalhoSistema = props => {
    return (
        <Cabecalho>
            CabecalhoSistema
        </Cabecalho>
    )
}


export default CabecalhoSistema
