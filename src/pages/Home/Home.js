import React, { useEffect, useState } from "react";
import "./home.css";
import { getLista } from "service/todoService";

const Home = () => {
  const [lista, setlista] = useState([]);
  const [loding, setloding] = useState(true);
  // componentDidMount
  useEffect(() => {
    const pegarLista = async () => {
      const lista = await getLista();
      setloding(false);
      setlista(lista);
    };

    pegarLista();
  }, []);

  if (loding) {
    return <div>Loading...</div>;
  }
  return <div>{JSON.stringify(lista)}</div>;
};

export default Home;
