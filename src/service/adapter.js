import axios from "axios";
import { API_HOST } from "core/constants";

const getToken = () => {}

export const withToken = fn => async params => {
  if (!getToken()) return;
  const token = getToken();
  return await fn({ token, ...params });
};

const handleErrors = error => {
  const { response } = error || {};
  console.error(response);
  throw error;
};

export const get = async ({ path }) => {
  const url = `${API_HOST}${path}`;

  return await axios
    .get(url)
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
};

export const post = async ({ path, data }) => {
  const url = `${API_HOST}${path}`;
  return await axios
    .post(url, data)
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
};

export const postReturningQR = async ({ path, data }) => {
  const url = `${API_HOST}${path}`;
  return await axios({
    url,
    data,
    method: "POST",
    responseType: "blob"
  })
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
};

export const authenticatedPatch = withToken(async ({ path, data, token }) => {
  const url = `${API_HOST}${path}`;
  const auth = "Bearer " + token;

  return await axios
    .patch(url, data, {
      headers: { Authorization: auth }
    })
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
});

export const authenticatedPut = withToken(async ({ path, data, token }) => {
  const url = `${API_HOST}${path}`;
  const auth = "Bearer " + token;

  return await axios
    .put(url, data, {
      headers: {
        Authorization: auth
      }
    })
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
});

export const authenticatedDelete = withToken(async ({ path, token }) => {
  const url = `${API_HOST}${path}`;
  const auth = "Bearer " + token;

  return await axios
    .delete(url, {
      headers: { Authorization: auth }
    })
    .then(function(response) {
      if (response.status === 204) {
        return {};
      }

      if (response.data) {
        return response.data;
      }
    })
    .catch(handleErrors);
});

export const authenticatedGet = withToken(async ({ path, token }) => {
  const url = `${API_HOST}${path}`;
  const auth = "Bearer " + token;

  return await axios({
    url,
    method: "GET",
    headers: { Authorization: auth }
  })
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
});

export const authenticatedPost = withToken(async ({ path, data, token }) => {
  const url = `${API_HOST}${path}`;
  const auth = "Bearer " + token;

  return await axios
    .post(url, data, {
      headers: { Authorization: auth }
    })
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
});

export const authenticatedGetFile = withToken(async ({ path, token }) => {
  const url = `${API_HOST}${path}`;
  const auth = "Bearer " + token;

  return await axios({
    url,
    method: "GET",
    responseType: "blob",
    headers: { Authorization: auth }
  })
    .then(function(response) {
      if (response.data) {
        return response.data;
      }
      if (response.status === 204) {
        return [];
      }
    })
    .catch(handleErrors);
});

export const authenticatedPostFile = withToken(
  async ({ path, data, config = {}, token }) => {
    const url = `${API_HOST}${path}`;
    const auth = "Bearer " + token;

    return await axios
      .post(url, data, {
        headers: { Authorization: auth },
        ...config
      })
      .then(function(response) {
        if (response.data) {
          return response.data;
        }
        if (response.status === 204) {
          return [];
        }
      })
      .catch(handleErrors);
  }
);
