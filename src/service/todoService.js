import { get } from 'service/adapter';


export const getLista = async () => {
    const url = '/todos/'
    return await get({path: url})
}
